document.getElementById('formulario').addEventListener('submit',cadastraVeiculo);

function cadastraVeiculo(e){
  var carro = document.getElementById('modeloCarro').value;
  var placa = document.getElementById('placaCarro').value;
  var time = new Date();

  carro = {
    modelo: carro,
    placa: placa,
    hora: time.getHours(),
    minutos: time.getMinutes()
  };

  if(localStorage.getItem('patio') === null){
    var carros = [];
    carros.push(carro);
    localStorage.setItem('patio', JSON.stringify(carros));
  }else{
    var carros = JSON.parse(localStorage.getItem('patio'));
    carros.push(carro);
    localStorage.setItem('patio', JSON.stringify(carros));
  }
  mostrarPatio();
  e.preventDefault();
}

function mostrarPatio(){

  var carros = JSON.parse(localStorage.getItem('patio'));
  var carrosResultado = document.getElementById('resultado');

  carrosResultado.innerHTML = '';
  for(var i = 0; i < carros.length; i++){
    var modelo = carros[i].modelo;
    var placa = carros[i].placa;
    var hora = carros[i].hora;
    var minutos = (carros[i].minutos < 10)? '0'+carros[i].minutos : carros[i].minutos;
    
    carrosResultado.innerHTML += '<tr><td>'+modelo+'</td><td>'
                                            +placa+'</td><td>'
                                            +hora+':'+minutos+
                                            '<td><button type="button" class="btn btn-danger" onclick="openModal(\''+placa+'\',\''+modelo+'\')">Finalizar</button></td>'+'</td><tr>';
  }

}

function finalizar(placa){
  closeModal();
  var carros = JSON.parse(localStorage.getItem('patio'));
  for(var i = 0; i < carros.length; i++){
    if(carros[i].placa == placa){
      carros.splice(i,1);

    }
    localStorage.setItem('patio',JSON.stringify(carros));
  }
  mostrarPatio();

}

var btn = document.getElementById('modalBtn');
var modal = document.getElementById('pagamentoModal');
var closebtn = document.getElementsByClassName("close")[0];


closebtn.addEventListener('click',closeModal);
window.addEventListener('click',outsideClick);

function openModal(placa,modelo){
  modal.style.display = 'block';
  var elementoPai = document.getElementById('modalContent');
  var div = document.createElement('DIV');
  var label = document.createElement('LABEL');
  var text = document.createTextNode('Modelo:'+modelo);

  label.appendChild(text);
  div.appendChild(label);
  elementoPai.appendChild(div);

  var elementoPai = document.getElementById('modalContent');
  var div = document.createElement('DIV');
  var label = document.createElement('LABEL');
  var text = document.createTextNode('Placa:'+placa);

  label.appendChild(text);
  div.appendChild(label);
  elementoPai.appendChild(div);

  var button = document.createElement('BUTTON');
  button.innerHTML ='Finalizar';
  button.setAttribute('class', 'btn btn-danger');
  button.setAttribute('id', 'finalizar');
  elementoPai.appendChild(button);

  var btn = document.getElementById('finalizar');
  btn.addEventListener('click', ()=>finalizar(placa));

}

function closeModal(){
  modal.style.display = 'none';
  var list = document.getElementById('modalContent');
  console.log(list);
  while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
}
function outsideClick(e){
  if(e.target == modal){
    modal.style.display = 'none';
    var list = document.getElementById('modalContent');
    console.log(list);
    while (list.hasChildNodes()) {
          list.removeChild(list.firstChild);
      }
  }

}
